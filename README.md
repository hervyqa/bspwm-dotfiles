# Bspwm Configuration

Simple configuration for Archlabs Bspwm. I using Adapta-dark as default theme.

## Requirement

- [Archlabs] or other Arch based distro

---

## Installation.

  * Update system
      `sudo pacman -Syyu`
  * install packages

      ```./install.sh```

  * optional packages, but you must install `st-luks-git` as default terminal. if you using Archlabs install with baph or other AUR helper.

      ```./aur.sh```

---

## Screenshot.

### 1. Boring wallpaper

  * xsetroot solid.

    ![](screenshot/desktop.png)

### 2. Terminal.

  * st + tmux + vim + ranger (super + return).

    ![](screenshot/terminal.png)

  * rofi menu (super + space).

    ![](screenshot/rofi.png)

  * networkmanager_dmenu (super + w, w).

    ![](screenshot/dmenu-wifi.png)

### 3. Adapta theme for QT and GTK.

  * kvantum + qt5ct + lxappearance.

    ![](screenshot/look.png)

  * inkscape (super + d, i).

    ![](screenshot/inkscape.png)

  * gimp (super + d, g).

    ![](screenshot/gimp.png)

### 4. Other.

  * qutebrowser (super + w, q).

    ![](screenshot/qutebrowser.png)

  * blender (super + d, b) with adapta-dark theme.

    ![](screenshot/blender.png)

  * libreoffice writer (super + r, w).

    ![](screenshot/writer.png)

  * libreoffice impress (super + r, i).

    ![](screenshot/impress.png)

Thanks for Watching. :blush:

See on my blog **[Hervyqa.com]** for design references.

[Archlabs]:https://archlabsblog.wordpress.com
[Hervyqa.com]:https://hervyqa.com
