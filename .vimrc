" Sane vim defaults for ArchLabs

scriptencoding utf8

" Arch defaults
runtime! archlinux.vim

" system clipboard (requires +clipboard)
set clipboard^=unnamed,unnamedplus

filetype plugin on

" additional settings
set modeline           " enable vim modelines
set hlsearch           " highlight search items
set incsearch          " searches are performed as you type
set number             " enable line numbers
set confirm            " ask confirmation like save before quit.
set wildmenu           " Tab completion menu when using command mode
set expandtab          " Tab key inserts spaces not tabs
set softtabstop=4      " spaces to enter for each tab
set shiftwidth=4       " amount of spaces for indentation
set shortmess+=aAcIws  " Hide or shorten certain messages

" additional settings 2
set noswapfile         " No swap
set nobackup           " No backup
set noerrorbells       " No annoying
set novisualbell       " Don't blink
set relativenumber     " scroll number
set ruler
set scrolloff=5        " Keep 10 lines (top/bottom) for scope
set splitbelow         " Vertical split to below
set splitright         " Horizontal split to right
set smarttab
set updatetime=100     " Quickly realtime update
set undofile
set undolevels=50000
set t_Co=256           " set terminal 256 color
set signcolumn=yes     " git sign

let g:netrw_altv = 1
let g:netrw_liststyle = 3
let g:netrw_browse_split = 3

" ------ leader mapping ------

let g:mapleader = "\<Space>"

" ------ enable additional features ------

" enable mouse
set mouse=a
if has('mouse_sgr')
    " sgr mouse is better but not every term supports it
    set ttymouse=sgr
endif

" syntax highlighting
syntax enable

set linebreak breakindent
set list listchars=tab:>>,trail:~

" colorsheme adapta midnight
let g:adapta_theme = 'midnight'
colorscheme adapta

if $TERM !=? 'linux'
    set termguicolors
    " true colors in terminals (neovim doesn't need this)
    if !has('nvim') && !($TERM =~? 'xterm' || &term =~? 'xterm')
        let $TERM = 'xterm-256color'
        let &term = 'xterm-256color'
    endif
    if has('multi_byte') && $TERM !=? 'linux'
        set listchars=tab:»»,trail:•
        set fillchars=vert:┃ showbreak=↪
    endif
endif

" change cursor shape for different editing modes, neovim does this by default
if !has('nvim')
    if exists('$TMUX')
        let &t_SI = "\<Esc>Ptmux;\<Esc>\e[5 q\<Esc>\\"
        let &t_SR = "\<Esc>Ptmux;\<Esc>\e[4 q\<Esc>\\"
        let &t_EI = "\<Esc>Ptmux;\<Esc>\e[2 q\<Esc>\\"
    else
        let &t_SI = "\e[6 q"
        let &t_SR = "\e[4 q"
        let &t_EI = "\e[2 q"
    endif
endif

" ------ commands ------

command! D Explore
command! R call <SID>ranger()
command! Q call <SID>quitbuffer()
command! -nargs=1 B :call <SID>bufferselect("<args>")
command! W execute 'silent w !sudo tee % >/dev/null' | edit!

" ------ basic maps ------

" open ranger as a file chooser using the function below
nnoremap <leader>R :call <SID>ranger()<CR>

"" match string to switch buffer
"nnoremap <Leader>b :let b:buf = input('Match: ')<Bar>call <SID>bufferselect(b:buf)<CR>

" change windows with ctrl+(hjkl)
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
nnoremap <BS> <C-W><C-H>

" alt defaults
nnoremap 0 ^
nnoremap Y y$
nnoremap n nzzzv
nnoremap N Nzzzv
nnoremap <Tab> ==1j

" re-visual text after changing indent
vnoremap > >gv
vnoremap < <gv

" toggle line numbers, nn (no number)
nnoremap <silent> <Leader>nn :set number!<CR>

" gj/k but preserve numbered jumps ie: 12j or 45k
nmap <buffer><silent><expr>j v:count ? 'j' : 'gj'
nmap <buffer><silent><expr>k v:count ? 'k' : 'gk'

" open a terminal in $PWD
nnoremap <silent> <Leader>tt :terminal<CR>

" tab control
nnoremap <silent> <M-j> :tabmove -1<CR>
nnoremap <silent> <M-k> :tabmove +1<CR>
nnoremap <silent> <Leader>te :tabnew<CR>
nnoremap <silent> <Leader>tn :tabnext<CR>
nnoremap <silent> <Leader>tf :tabfirst<CR>
nnoremap <silent> <Leader>tp :tabprevious<CR>

" close/exit current buffer and/or tab
"nnoremap <silent> <Leader>q :B<CR>:silent tabclose<CR>gT
nnoremap <silent> <Leader>q :q<CR>

" force close buffer
nnoremap <silent> <leader>Q :bd<CR>

" open a new tab in the current directory with netrw
nnoremap <silent> <Leader>- :tabedit <C-R>=expand("%:p:h")<CR><CR>

" split the window vertically and horizontally
nnoremap _ <C-W>s<C-W><Down>
nnoremap <Bar> <C-W>v<C-W><Right>

" quick reload .vimrc
nnoremap <silent> <leader>r :source $MYVIMRC<CR>

" save
nnoremap <silent> <leader>w :w<CR>

" quick open files
nnoremap <silent> <leader>ev :e $MYVIMRC<CR>
nnoremap <silent> <leader>ez :e ~/.zshrc<CR>
nnoremap <silent> <leader>et :e ~/.tmux.conf<CR>
nnoremap <silent> <leader>ex :e ~/.Xresources<CR>
nnoremap <silent> <leader>eb :e ~/.config/bspwm/bspwmrc<CR>
nnoremap <silent> <leader>es :e ~/.config/sxhkd/sxhkdrc<CR>
nnoremap <silent> <leader>ep :e ~/.config/polybar/config<CR>

" equally
map = <C-w>=

" arrow keys resize windows
nnoremap <silent> <Left> :vertical resize +2<CR>
nnoremap <silent> <Right> :vertical resize -2<CR>
nnoremap <silent> <Up> :resize +2<CR>
nnoremap <silent> <Down> :resize -2<CR>

" yank from vim to clipboard
vnoremap <leader>y "+ygv"*y
" cut from vim to clipboard
vnoremap <leader>x "+ygv"*ygvd

" ------ autocmd ------

" Reload changes if file changed outside of vim requires autoread
augroup load_changed_file
    autocmd!
    autocmd FocusGained,BufEnter * if mode() !=? 'c' | checktime | endif
    autocmd FileChangedShellPost * echo "Changes loaded from source file"
augroup END

" when quitting a file, save the cursor position
augroup save_cursor_position
    autocmd!
    autocmd BufReadPost * call setpos(".", getpos("'\""))
augroup END

" when not running in a console or a terminal that doesn't support 256 colors
" enable cursorline in the currently active window and disable it in inactive ones
if $DISPLAY !=? '' && &t_Co == 256
    augroup cursorline
        autocmd!
        autocmd VimEnter,WinEnter,BufWinEnter * setlocal cursorline
        autocmd WinLeave * setlocal nocursorline
    augroup END
endif

" ------ adv maps ------


" strip trailing whitespace, ss (strip space)
nnoremap <silent> <Leader>ss
    \ :let b:_p = getpos(".") <Bar>
    \  let b:_s = (@/ != '') ? @/ : '' <Bar>
    \  %s/\s\+$//e <Bar>
    \  let @/ = b:_s <Bar>
    \  nohlsearch <Bar>
    \  unlet b:_s <Bar>
    \  call setpos('.', b:_p) <Bar>
    \  unlet b:_p <CR>

" global replace
vnoremap <Leader>sw "hy
    \ :let b:sub = input('global replacement: ') <Bar>
    \ if b:sub !=? '' <Bar>
    \   let b:rep = substitute(getreg('h'), '/', '\\/', 'g') <Bar>
    \   execute '%s/'.b:rep."/".b:sub.'/g' <Bar>
    \   unlet b:sub b:rep <Bar>
    \ endif <CR>
nnoremap <Leader>sw
    \ :let b:sub = input('global replacement: ') <Bar>
    \ if b:sub !=? '' <Bar>
    \   execute "%s/<C-r><C-w>/".b:sub.'/g' <Bar>
    \   unlet b:sub <Bar>
    \ endif <CR>

" prompt before each replace
vnoremap <Leader>cw "hy
    \ :let b:sub = input('interactive replacement: ') <Bar>
    \ if b:sub !=? '' <Bar>
    \   let b:rep = substitute(getreg('h'), '/', '\\/', 'g') <Bar>
    \   execute '%s/'.b:rep.'/'.b:sub.'/gc' <Bar>
    \   unlet b:sub b:rep <Bar>
    \ endif <CR>

nnoremap <Leader>cw
    \ :let b:sub = input('interactive replacement: ') <Bar>
    \ if b:sub !=? '' <Bar>
    \   execute "%s/<C-r><C-w>/".b:sub.'/gc' <Bar>
    \   unlet b:sub <Bar>
    \ endif <CR>

" highlight long lines, ll (long lines)
let w:longlines = matchadd('ColorColumn', '\%'.&textwidth.'v', &textwidth)
nnoremap <silent> <Leader>ll
    \ :if exists('w:longlines') <Bar>
    \   silent! call matchdelete(w:longlines) <Bar>
    \   echo 'Long line highlighting disabled'
    \   <Bar> unlet w:longlines <Bar>
    \ elseif &textwidth > 0 <Bar>
    \   let w:longlines = matchadd('ColorColumn', '\%'.&textwidth.'v', &textwidth) <Bar>
    \   echo 'Long line highlighting enabled'
    \ <Bar> else <Bar>
    \   let w:longlines = matchadd('ColorColumn', '\%80v', 81) <Bar>
    \   echo 'Long line highlighting enabled'
    \ <Bar> endif <CR>

" local keyword jump
nnoremap <Leader>fw
    \ [I:let b:jump = input('Go To: ') <Bar>
    \ if b:jump !=? '' <Bar>
    \   execute "normal! ".b:jump."[\t" <Bar>
    \   unlet b:jump <Bar>
    \ endif <CR>


" quit the current buffer and switch to the next
" without this vim will leave you on an empty buffer after quiting the current
function! <SID>quitbuffer() abort
    let l:bf = bufnr('%')
    let l:pb = bufnr('#')
    if buflisted(l:pb)
        buffer #
    else
        bnext
    endif
    if bufnr('%') == l:bf
        new
    endif
    if buflisted(l:bf)
        execute('bdelete! ' . l:bf)
    endif
endfunction

" switch active buffer based on pattern matching
" if more than one match is found then list the matches to choose from
function! <SID>bufferselect(pattern) abort
    let l:bufcount = bufnr('$')
    let l:currbufnr = 1
    let l:nummatches = 0
    let l:matchingbufnr = 0
    " walk the buffer count
    while l:currbufnr <= l:bufcount
        if (bufexists(l:currbufnr))
            let l:currbufname = bufname(l:currbufnr)
            if (match(l:currbufname, a:pattern) > -1)
                echo l:currbufnr.': '.bufname(l:currbufnr)
                let l:nummatches += 1
                let l:matchingbufnr = l:currbufnr
            endif
        endif
        let l:currbufnr += 1
    endwhile

    " only one match
    if (l:nummatches == 1)
        execute ':buffer '.l:matchingbufnr
    elseif (l:nummatches > 1)
        " more than one match
        let l:desiredbufnr = input('Enter buffer number: ')
        if (strlen(l:desiredbufnr) != 0)
            execute ':buffer '.l:desiredbufnr
        endif
    else
        echoerr 'No matching buffers'
    endif
endfunction

" open ranger as a file chooser
function! <SID>ranger()
    let l:temp = tempname()
    execute 'silent !xterm -e ranger --choosefiles='.shellescape(l:temp).' $PWD'
    if !filereadable(temp)
        redraw!
        return
    endif
    let l:names = readfile(l:temp)
    if empty(l:names)
        redraw!
        return
    endif
    execute 'edit '.fnameescape(l:names[0])
    for l:name in l:names[1:]
        execute 'argadd '.fnameescape(l:name)
    endfor
    redraw!
endfunction
" ------ vim-plugin ------ {{{

" - vim-airline
let g:airline_theme='tomorrow'
"let g:airline_theme='onedark'

" - vim-ale
let g:ale_sign_column_always=1
let g:ale_completion_enabled=1
" bind f8 to fixing problems with ale
nmap <F8> <Plug>(ale_fix)

" - vim-align

" - vim-csound

" - vim-ctrlp
"   open files
let g:ctrlp_map = '<leader>p'
"   open buffer
nnoremap <silent> <leader>b :CtrlPBuffer<CR>
"   open MRU recent files
nnoremap <silent> <leader>m :CtrlPMRU<CR>

set wildignore+=*/.git/*,*/.hg/*,*/.svn/*        " linux/macosx

" - vim-doxygentoolkit
" python highlight
let python_highlight_all = 1

" - vim-easymotion
let g:EasyMotion_keys='asdfghjkl;'
let g:EasyMotion_smartcase = 1
let g:EasyMotion_use_smartsign_us = 1
nmap s <Plug>(easymotion-s)
xmap s <Plug>(easymotion-s)
omap z <Plug>(easymotion-s)

" - vim-fugitive
noremap <Leader>ga :Gwrite<CR>
noremap <Leader>gc :Gcommit<CR>
noremap <Leader>gsh :Gpush<CR>
noremap <Leader>gll :Gpull<CR>
noremap <Leader>gs :Gstatus<CR>
noremap <Leader>gb :Gblame<CR>
noremap <Leader>gd :Gvdiff<CR>
noremap <Leader>gr :Gremove<CR>

" - vim-gitgutter (default mapping)
nmap ghp <Plug>GitGutterPreviewHunk
nmap ghs <Plug>GitGutterStageHunk
nmap ghu <Plug>GitGutterUndoHunk
nmap [c <Plug>GitGutterPrevHunk
nmap ]c <Plug>GitGutterNextHunk
omap ic <Plug>GitGutterTextObjectInnerPending
omap ac <Plug>GitGutterTextObjectOuterPending
xmap ic <Plug>GitGutterTextObjectInnerVisual
xmap ac <Plug>GitGutterTextObjectOuterVisual

" - vim-indent-object

" - vim-jad

" - vim-jedi
"default: <ctrl-space> start completion

" - nerdtree
let NERDTreeMinimalUI=1
let NERDTreeShowHidden=1
let NERDTreeShowBookmarks=1
let NERDTreeHijackNetrw=1
let NERDTreeHighlightCursorline=1
let NERDTreeDirArrows=1

map <silent> <F12> :NERDTreeToggle<CR><C-W>=
let NERDTreeIgnore=['\.vim$','\.git$','\.pyc$','^__pycache__$', '^node_modules$']
autocmd FileType nerdtree setlocal signcolumn=no nocursorcolumn

" - nerdcommenter
" leader cc = comment
" leader cu = uncomment

" - vim-omnicompletion
map <C-F6> :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<CR>

" - vim-pastie

" - vim-project
let loaded_project = 1

" - supertab
"   rolling
let g:SuperTabMappingForward = '<s-tab>'
let g:SuperTabMappingBackward = '<tab>'
let g:SuperTabLongestHighlight = 1
let g:SuperTabLongestEnhanced = 1
let g:SuperTabDefaultCompletionType = "<c-x><c-u>"

" - vim-surround
" surround : ds cs ys
" surround old new

" - vim-syntastic
"   recomended setting
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" - vim-tabular
let g:tabular_loaded = 1

" - vim-tagbar
nnoremap <silent> <F9> :TagbarToggle<CR><C-w>=
let g:tagbar_autopreview      = 0
let g:tagbar_indent           = 1
let g:tagbar_show_linenumbers = 2
let g:tagbar_show_visibility  = 1
let g:tagbar_singleclick      = 1
let g:tagbar_sort             = 0


" - vim-taglist
nnoremap <silent> <F7> :TlistToggle<CR>

" - vim-ultisnips
let g:UltiSnipsUsePythonVersion = 3     "python 3.x
let g:UltiSnipsNoPythonWarning = 1

inoremap <c-x><c-k> <c-x><c-k>

" - vim-vital

" ------ filetype ------
" css
autocmd FileType scss,sass,css setlocal expandtab shiftwidth=2 softtabstop=2 textwidth=0
" html5
autocmd FileType html,jinja.html setlocal expandtab shiftwidth=2 softtabstop=2 textwidth=0
" javascript
autocmd FileType javascript,jst,ezt setlocal expandtab shiftwidth=2 softtabstop=2
autocmd FileType javascript.jsx set syntax=javascript
" markdown
autocmd FileType markdown setlocal expandtab shiftwidth=2 softtabstop=2 fo+=t fo-=l
let g:vim_markdown_conceal             = 0
let g:mkdp_auto_close                  = 0
let g:vim_markdown_folding_disabled    = 1
let g:vim_markdown_math                = 1
let g:vim_markdown_frontmatter         = 1
let g:vim_markdown_toml_frontmatter    = 1
let g:vim_markdown_json_frontmatter    = 1
let g:vim_markdown_fenced_languages    = ['csharp = cs']
let g:vim_markdown_auto_insert_bullets = 0
" php
autocmd FileType php setlocal expandtab shiftwidth=4 softtabstop=4
" python
autocmd FileType python setlocal expandtab shiftwidth=4 softtabstop=4

