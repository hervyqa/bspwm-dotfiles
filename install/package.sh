#!/bin/bash
sudo pacman -S \
a52dec faac faad2 flac jasper lame libdca libdv libmad libmpeg2 libtheora libvorbis libxv wavpack x264 xvidcore \
atool lha arj unarj unace p7zip unrar cpio zip unzip lzop xarchiver thunar \
adobe-source-{code,serif}-pro-fonts \
ttf-fantasque-sans-mono otf-fantasque-sans-mono ttf-roboto ttf-opensans ttf-font-awesome \
nitrogen hsetroot lxappearance qt5ct kvantum-qt5 \
adapta-gtk-theme adapta-kde kvantum-theme-adapta \
arandr \
rxvt-unicode \
wpa_supplicant archlabs-networkmanager-dmenu \
trash-cli \
udiskie \
xclip xsel xorg-xclipboard \
mpv cmus mpd ncmpcpp \
ranger pcmanfm \
w3m \
ffmpegthumbnailer \
mupdf \
feh \
scrot \
git neovim python-neovim ctags \
gvfs-mtp android-tools \
openssh sshfs \
hwinfo mediainfo glances gtop \
gimp inkscape python2-lxml python-lxml python2-pyserial python-pyserial ghostscript \
rtorrent \
aria2 \
neofetch \
bleachbit \
exo \
qutebrowser \
